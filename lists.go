package postup

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
)

// List Structure
type List struct {
	ID                int64   `json:"listId,omitempty"`
	Title             string  `json:"title"` // Required
	FriendlyTitle     string  `json:"friendlyTitle,omitempty"`
	Description       string  `json:"description,omitempty"`
	Populated         bool    `json:"populated"`    // Required
	PublicSignUp      bool    `json:"publicSignup"` // Required
	GlobalUnsubscribe bool    `json:"globalUnsub"`  // Required
	Query             string  `json:"query,omitempty"`
	CategoryID        int64   `json:"categoryId,omitempty"`
	BlockDomains      string  `json:"blockDomains,omitempty"` // Space separated string
	SeedListID        int64   `json:"seedListId,omitempty"`
	Created           string  `json:"createTime,omitempty"`
	Creator           string  `json:"creator,omitempty"`
	ExternalID        string  `json:"externalId,omitempty"`
	Custom            string  `json:"custom1,omitempty"`
	Channel           string  `json:"channel"`     // Required - Email=E, SMS=S, Push=P
	CountRecipients   bool    `json:"countRecips"` // Required
	TestMessageList   bool    `json:"testMessageList,omitempty"`
	BrandIDs          []int64 `json:"brandIds,omitempty"`
	Counts            Counts  `json:"-"`
}

// Counts statistics for a list
type Counts struct {
	ListID          int64            `json:"listId"`
	Title           string           `json:"title"`
	Type            string           `json:"type"`
	Counts          map[string]int64 `json:"counts"`
	TotalRecipients int64            `json:"totalRecips"`
	BlockedDomains  []string         `json:"blockDomains"`
}

// NewList create a new list instance
func NewList() *List {
	return new(List)
}

// FindList finds a list by ID
func FindList(ID int64) (*List, error) {
	// Send the request to the API
	response, err := postUp.request(methodGet, pathList+"/"+strconv.FormatInt(ID,10) , nil)
	if err != nil {
		return nil, err
	}

	// Unpack the response
	list := new(List)
	err = json.Unmarshal(response, list)
	if err != nil {
		return nil, err
	}

	return list, nil
}

// GetLists based on a variable
func GetLists(by string, value interface{}) (*[]List, error) {
	// Build the request URL
	url := pathList + "?"

	// Set the search parameter based on the type
	switch by {
	case "channel":
		url = url + "channel=" + fmt.Sprintf("%v", value)
	case "brand":
		url = url + "brandid=" + fmt.Sprintf("%v", value)
	case "category":
		url = url + "categoryid=" + fmt.Sprintf("%v", value)
	default:
		return nil, errors.New("list search method is not supported")
	}

	// Send the request to the API
	response, err := postUp.request(methodGet, url, nil)
	if err != nil {
		return nil, err
	}

	// Unpack the response
	//TODO: figure out where in the response we can get the right size
	list := new([]List)
	err = json.Unmarshal(response, &list)
	if err != nil {
		return nil, err
	}

	return list, nil
}

// Create a new list in PostUp
func (l *List) Create() error {
	// Pack the list details
	details, err := json.Marshal(l)
	if err != nil {
		return err
	}

	// Send the request to the API
	response, err := postUp.request(methodPost, pathList, details)
	if err != nil {
		return err
	}

	// Unpack the response
	list := new(List)
	err = json.Unmarshal(response, list)
	if err != nil {
		return err
	}

	// Update the list
	l = list

	return nil
}

// Save a list in PostUp
func (l *List) Save() error {
	// Pack the users details
	details, err := json.Marshal(l)
	if err != nil {
		return err
	}

	// Send the request to the API
	response, err := postUp.request(methodPut, pathList+"/"+strconv.FormatInt(l.ID,10) , details)
	if err != nil {
		return err
	}

	// Unpack the response
	list := new(List)
	err = json.Unmarshal(response, list)
	if err != nil {
		return err
	}

	// Update the list
	l = list

	return nil
}

// GetStats get the statistics for a list
func (l *List) GetStats() error {

	// Send the request to the API
	response, err := postUp.request(methodGet, pathList+"/"+strconv.FormatInt(l.ID,10) +"/counts", nil)
	if err != nil {
		return err
	}

	// Unpack the response
	counts := new(Counts)
	err = json.Unmarshal(response, counts)
	if err != nil {
		return err
	}

	// Update the list
	l.Counts = *counts

	return nil
}
