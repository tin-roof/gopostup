package postup

import (
	"bytes"
	"net/http"
	"encoding/base64"
	"io/ioutil"
	
)

// API URL
const api string = "https://api.postup.com/api"

// Request Paths
const (
	pathRecipient    = "/recipient"
	pathSubscription = "/listsubscription"
	pathList         = "/list"
)

// Request methods
const (
	methodGet  = "GET"
	methodPost = "POST"
	methodPut  = "PUT"
)

// PostUp defines the post up type
type PostUp struct {
	system struct {
		username      string
		password      string
		authorization string
	}
}

// postUp instance
var postUp PostUp

// Init initialize a PostUp instance
func Init(username string, password string) bool {
	// Create the PostUp instance

	postUp.system.username = username
	postUp.system.password = password

	// Set the auth token
	postUp.setAuthorizationToken()

	return true
}

// setAuthorizationToken builds the authorization token for the user
func (pu *PostUp) setAuthorizationToken() {
	pu.system.authorization = base64.StdEncoding.EncodeToString([]byte(pu.system.username + ":" + pu.system.password))
}

func (pu *PostUp) request(method string, endpoint string, body []byte) ([]byte, error) {
	// auth header = authorization: Basic YWRtaW5AY2xpZW50LmNvbTp0ZXN0MTIzNCE=

	// Create a new request
	req, err := http.NewRequest(method, api+endpoint, bytes.NewBuffer(body))

	// Add headers
	req.Header.Add("authorization", "Basic "+pu.system.authorization)

	// Make the API request
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	// Get the response body
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}
