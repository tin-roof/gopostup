# goPostUp

## Import
```go
    import postup "gitlab.com/tin-roof/gopostup"
```

## Usage

### Initialize PostUp
```go
    // Initialize postup
    postup.Init("account@email.com", "password")
```

### Create a new recipient
```go
    // Create a Recipient object
    recipient := postup.NewRecipient()
    
    // Add Details to the recipient
    // ...
    
    // Create the recipient
    err := recipient.Create()
    if err != nil {
        // Failed to create the new recipient
    }
```

### Find a recipient
```go
    // Create a recipient object
    recipient, err := postup.FindRecipient("account@email.com")
    if err != nil {
        // Failed to find a recipient
    }
```

### Update a recipient
```go
    // Find a recipient
    recipient, err := postup.FindRecipient("account@email.com")
    if err != nil {
        // Failed to find a recipient
    }
    
    // Update recipient details
    // ...
    
    // Save the changes back to PostUp
    err := recipient.Save()
    if err != nil {
        // Failed to save the update
    }
```