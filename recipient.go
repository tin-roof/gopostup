package postup

import (
	"encoding/json"
	"strconv"
	"errors"

)

// @TODO: Work with the privacy functions https://apidocs.postup.com/docs/return-recipient-privacy-data

// Recipient PostUp recipient structure
type Recipient struct {
	ID                int64    `json:"recipientId,omitempty"` // PostUp system ID
	Address           string   `json:"address"`               // Required :: email address
	ExternalID        string   `json:"externalId"`            // Required :: External system id
	Channel           string   `json:"channel"`               // Required :: "E" = Email, "S" = SMS, "P" = Push
	Status            string   `json:"status,omitempty"`      // "N" = Normal, "U" = Unsubscribe, "H" = Held.
	SourceDescription string   `json:"sourceDescription,omitempty"`
	Comment           string   `json:"comment,omitempty"`
	ImportID          int64    `json:"importId,omitempty"`
	Password          string   `json:"password,omitempty"`
	Carrier           string   `json:"carrier,omitempty"`
	SourceSignUp      string   `json:"sourceSignupDate,omitempty"`
	SignUpIP          string   `json:"signupIP,omitempty"`
	SignUpMethod      string   `json:"signupMethod,omitempty"`
	ThirdPartySource  string   `json:"thirdPartySource,omitempty"`
	ThirdPartySignUp  string   `json:"thirdPartySignupDate,omitempty"`
	Joined            string   `json:"dateJoined,omitempty"`
	Unsubscribed      string   `json:"dateUnsub,omitempty"`
	Demographics      []string `json:"demographics,omitempty"` // slice of key values with '=' delimiter ex: FirstName=John **NOT** "FirstName" = "John"
}

// Subscription details
type Subscription struct {
	MailingID    int64  `json:"mailingId,omitempty"`
	RecipientId  int64  `json:"recipientId"`
	ListID       int64  `json:"listId"`
	Status       string `json:"Status,omitempty"` // Subscribed = "NORMAL" and Unsubscribed = "UNSUB"
	ListStatus   string `json:"listStatus,omitempty"`
	GlobalStatus string `json:"globalStatus,omitempty"`
	SourceID     string `json:"sourceId,omitempty"`
	Confirmed    bool   `json:"confirmed,omitempty"` // Confirmed = "true" or Unconfirmed = "false"
	Unsubscribed string `json:"dateUnsub,omitempty"`
	Subscribed   string `json:"dateJoined,omitempty"`
	Address      string `json:"address"`
}

// NewRecipient create an new recipient
func NewRecipient() *Recipient {
	// Create a new recipient
	return new(Recipient)
}

// FindRecipient looks up a recipient in PostUp
func FindRecipient(email string) (*Recipient, error) {
	// Send the request to the API
	response, err := postUp.request(methodGet, pathRecipient+"?address="+email, nil)
	if err != nil {
		return nil, err
	}
	
	
	// Unpack the response
	recipients := []Recipient{}
	err = json.Unmarshal(response, &recipients)
	if err != nil {
		return nil, err
	}

	// Check to see if any recipients were found
	if len(recipients) == 0 {
		return nil, errors.New("No recipient found")
	}

	return &recipients[0], nil
}

// Create a new recipient in PostUp
func (r *Recipient) Create() error {
	// Pack the users details
	details, err := json.Marshal(r)
	if err != nil {
		return err
	}

	response, err := postUp.request(methodPost, pathRecipient, details)
	if err != nil {
		return err
	}

	// Unpack the response
	recipient := new(Recipient)
	err = json.Unmarshal(response, recipient)
	if err != nil {
		return err
	}

	// Update the recipient
	r.ID = recipient.ID

	return nil
}

// Save an existing recipient
func (r *Recipient) Save() error {
	// Pack the users details
	details, err := json.Marshal(r)
	if err != nil {
		return err
	}

	// Send the request to the API
	response, err := postUp.request(methodPut, pathRecipient+"/"+strconv.FormatInt(r.ID,10), details)
	if err != nil {
		return err
	}

	// Unpack the response
	recipient := new(Recipient)
	err = json.Unmarshal(response, recipient)
	if err != nil {
		return err
	}

	// Update the recipient
	r = recipient

	return nil
}

// IsSubscribed checks to see if a user is subscribed to a list
func (r *Recipient) IsSubscribed(list *List) bool {
	// Send the request to the API
	response, err := postUp.request(methodGet, pathSubscription+"/"+strconv.FormatInt(list.ID,10)+"/"+strconv.FormatInt(r.ID,10), []byte(""))
	if err != nil {
		return false
	}

	// Unpack the response
	subscription := new(Subscription)
	err = json.Unmarshal(response, subscription)
	if err != nil {
		return false
	}

	// If the address is defined the subscription exists
	if subscription.Address != "" {
		return true
	}

	return false
}

// GetSubscriptions gets a list of subscriptions for a recipient
func (r *Recipient) GetSubscriptions() ([]Subscription, error) {

	//@TODO: get details for postup request

	// Send the request to the API
	response, err := postUp.request(methodGet, pathSubscription+"?recipid="+strconv.FormatInt(r.ID,10), []byte(""))
	if err != nil {
		return nil, err
	}

	// Unpack the response
	list := new([]Subscription)
	err = json.Unmarshal(response, list)
	if err != nil {
		return nil, err
	}

	return *list, nil
}

// Subscribe a recipient to a list
func (r *Recipient) Subscribe(list *List) (bool, error) {
	// Create a subscription
	sub := Subscription{
		RecipientId: r.ID,
		ListID:      list.ID,
		Status:      "NORMAL",
	}

	// Pack the subscription
	subBytes, err := json.Marshal(sub)
	if err != nil {
		return false, err
	}

	// Send the request to the API
	response, err := postUp.request(methodPost, pathSubscription, subBytes)
	if err != nil {
		return false, err
	}

	// Unpack the response
	newSubscription := new(Subscription)
	err = json.Unmarshal(response, newSubscription)
	if err != nil {
		return false, err
	}

	// Make sure the subscription worked
	if newSubscription.MailingID != 0 {
		return true, nil
	}

	// Subscription failed
	return false, errors.New("failed to add subscription")
}

// Unsubscribe a recipient to a list
func (r *Recipient) Unsubscribe(list *List) (bool, error) {
	// Create a subscription
	sub := Subscription{
		RecipientId: r.ID,
		ListID:      list.ID,
		Status:      "UNSUB",
	}

	// Pack the subscription
	subBytes, err := json.Marshal(sub)
	if err != nil {
		return false, err
	}

	// Send the request to the API
	response, err := postUp.request(methodPut, pathSubscription, subBytes)
	if err != nil {
		return false, err
	}

	// Unpack the response
	newSubscription := new(Subscription)
	err = json.Unmarshal(response, newSubscription)
	if err != nil {
		return false, err
	}

	// Make sure the subscription worked
	if newSubscription.MailingID != 0 {
		return true, nil
	}

	// Subscription failed
	return false, errors.New("failed to add subscription")
}

// UnsubscribeAll unsubscribe the recipient from all lists
func (r *Recipient) UnsubscribeAll() error {

	// Build request details
	details := []byte(`{"recipientId":` + strconv.FormatInt(r.ID,10) + `,"status":"U"}`)

	// Send the request to the API
	response, err := postUp.request(methodPut, pathRecipient+"/"+strconv.FormatInt(r.ID,10), details)
	if err != nil {
		return err
	}

	// Unpack the response
	newRecipient := new(Recipient)
	err = json.Unmarshal(response, newRecipient)
	if err != nil {
		return err
	}

	// Update the recipient
	r = newRecipient

	return nil
}
